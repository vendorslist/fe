# ui

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Login to vendor apps can be found at.
See BE Seed data [Configuration Reference](https://gitlab.com/vendorslist/be/-/blob/master/README.md).
