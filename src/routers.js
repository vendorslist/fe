import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from './components/DashboardView'
import Vendor from './components/VendorView'
import Login from './components/LoginView'

const routes = [
    {
        name: 'Login',
        path: '/login',
        component: Login
    },
    {
        name: 'Dashboard',
        path: '/',
        component: Dashboard
    },
    {
        name: 'Vendor',
        path: '/vendor',
        component: Vendor
    },
]

const router = new createRouter({
    history: createWebHistory(),
    routes
})


export default router;